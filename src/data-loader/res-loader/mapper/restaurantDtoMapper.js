    
    mapRestaurantDto = (inputJson)=>{
      
        var dtoJson = {};

        dtoJson.name = inputJson.name;
        dtoJson.location = inputJson.location;
        dtoJson.id = inputJson.id;
        dtoJson.menu_url = inputJson.menu_url;
        dtoJson.cuisines = inputJson.cuisines;
        dtoJson.user_rating = inputJson.user_rating;
        return dtoJson;
    }
module.exports ={
    mapRestaurantDto:mapRestaurantDto
} ;


