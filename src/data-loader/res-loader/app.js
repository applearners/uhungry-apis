//import {mapRestaurantDto} from "../mapper/restaurantDtoMapper";

let sourceFileLocation = "Data-Files/res2.json";
let  dtoMapperModule = require("./mapper/restaurantDtoMapper")

let express= require('express');
var  app = express();
var fs = require('fs');


getResJsonfromJsonFile =(filePath,callback) =>{

  let fileJsondata;
  fs.readFile(filePath, 'utf8', function (err, data) {
    if (err) throw err;
    fileJsondata = JSON.parse(data);

    callback(fileJsondata)
  });

}


getResData =(fileJson)=>{

  let restaurantsArr = [];
  fileJson.forEach((r1)=> {
      r1.restaurants.forEach( (r2 )=>{
      var dto = dtoMapperModule.mapRestaurantDto(r2.restaurant);
      restaurantsArr.push(dto);
     });
  });
 return restaurantsArr;
}

module.exports  = {
  readResJsonFile : getResJsonfromJsonFile,
  getRestaurants : getResData
}

app.get("/res",(req,res,next)=>{

  let data = getResJsonfromJsonFile(sourceFileLocation, successLoaded);

  function successLoaded (data) {
    var dtotoreturn = getResData(data);
    res.send(dtotoreturn);
  }
  
});

app.get("/res/str",(req,res,next)=>{

    res.send(JSON.stringify( fileJsondata));
  });


app.listen(3000,()=>{
console.log("res-loader app is listening to 3000...")
})